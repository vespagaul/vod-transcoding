class AddCdnUrlToClient < ActiveRecord::Migration[5.1]
  def change
    add_column :clients, :cdn_url, :string
  end
end
