class AddNotificationUrlToClient < ActiveRecord::Migration[5.1]
  def change
    add_column :clients, :notification_url, :string
  end
end
