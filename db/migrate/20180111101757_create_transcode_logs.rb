class CreateTranscodeLogs < ActiveRecord::Migration[5.1]
  def change
    create_table :transcode_logs, { id: false } do |t|
      t.string :transcode_key
      t.integer :status
      t.text :description

      t.timestamps
    end
  end
end
