class AddIndexToTranscodeLog < ActiveRecord::Migration[5.1]
  def change
    add_index :transcode_logs, :transcode_key
  end
end
