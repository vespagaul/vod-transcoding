class CreateClient < ActiveRecord::Migration[5.1]
  def change
    create_table :clients do |t|
      t.string :name
      t.string :token

      t.timestamps
      t.index :token, unique: true
    end
  end
end
