class AddGcsBucketAttributesToClients < ActiveRecord::Migration[5.1]
  def change
    add_column :clients, :bucket_type, :integer

    add_column :clients, :input_bucket_project_id, :string
    add_column :clients, :input_bucket_name, :string
    add_column :clients, :input_bucket_credentials, :text

    add_column :clients, :output_bucket_project_id, :string
    add_column :clients, :output_bucket_name, :string
    add_column :clients, :output_bucket_credentials, :text
  end
end
