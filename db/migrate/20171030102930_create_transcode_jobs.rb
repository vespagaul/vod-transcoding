class CreateTranscodeJobs < ActiveRecord::Migration[5.1]
  def change
    create_table :transcode_jobs, { id: false } do |t|
      t.string :transcode_key
      t.string :video_key
      t.string :client_token

      t.timestamps
      t.index :transcode_key, unique: true
    end
  end
end
