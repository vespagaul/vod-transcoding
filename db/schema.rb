# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180214040701) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "clients", force: :cascade do |t|
    t.string "name"
    t.string "token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "bucket_type"
    t.string "input_bucket_project_id"
    t.string "input_bucket_name"
    t.text "input_bucket_credentials"
    t.string "output_bucket_project_id"
    t.string "output_bucket_name"
    t.text "output_bucket_credentials"
    t.string "cdn_url"
    t.string "notification_url"
    t.index ["token"], name: "index_clients_on_token", unique: true
  end

  create_table "transcode_jobs", id: false, force: :cascade do |t|
    t.string "transcode_key"
    t.string "video_key"
    t.string "client_token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["transcode_key"], name: "index_transcode_jobs_on_transcode_key", unique: true
  end

  create_table "transcode_logs", id: false, force: :cascade do |t|
    t.string "transcode_key"
    t.integer "status"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["transcode_key"], name: "index_transcode_logs_on_transcode_key"
  end

end
