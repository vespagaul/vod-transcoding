# VOD Transcoding

Tools to transcode multi-format video to HLS formatted with multi-bitrate and thumbnails with 3 pixel resolutions.
Equipped with admin tools for managing users and viewing logs from each trancode video process.
Broadly speaking the way it works follows AWS Elastic Transcoder and Zencoder (https://www.brightcove.com/en/zencoder)