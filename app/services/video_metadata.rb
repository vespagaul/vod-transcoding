class VideoMetadata
  attr_reader :local_path, :key

  def initialize(source)
    @source = source
  end

  def ffprobe
    @ffprobe ||= Ffprober::Parser.from_file @source
  end

  def source_name
    File.basename(@source, ".*")
  end

  def source_path
    File.dirname(@source)
  end

  def video_stream
    @video_stream ||= ffprobe.video_streams.first
  end

  def is_portrait?
    return false unless video_stream

    video_is_portrait =  (video_stream.width < video_stream.height)

    if (video_stream.respond_to? :tags)
      rotation = video_stream.tags.try(:[], :rotate)

      if rotation && (rotation != "180")
        video_is_portrait = !video_is_portrait
      end
    end

    video_is_portrait
  end
end
