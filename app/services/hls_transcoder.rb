class HlsTranscoder
  class << self
    RENDER_CONFIGURATIONS = [
      # {resolution: "426x240", bitrate: "600k", audiorate: "32k"},
      # {resolution: "640x360", bitrate: "800k", audiorate: "64k"},
      # {resolution: "1280x720", bitrate: "1500k", audiorate: "64k"},
      # {resolution: "1920x1080", bitrate: "3000k", audiorate: "128k"}
      {resolution: "426x240", bitrate: "400k", audiorate: "32k"},
      {resolution: "640x360", bitrate: "600k", audiorate: "64k"},
      {resolution: "1280x720", bitrate: "1200k", audiorate: "64k"},
      {resolution: "1920x1080", bitrate: "1800k", audiorate: "128k"}
    ]

    SEGMENT_TARGET_DURATION = 5      # try to create a new segment every X seconds
    MAX_BITRATE_RATIO       = 1.07          # maximum accepted bitrate fluctuations
    RATE_MONITOR_BUFFER     = 1.5   # maximum buffer size between bitrate conformance checks
    STATIC_PARAMS           = "-c:a aac -ar 48000 -c:v libx264 -profile:v main -crf 22 -sc_threshold 0 -hls_playlist_type vod"

    def transcode(source)
      video_metadata = VideoMetadata.new(source)

      source_path = video_metadata.source_path
      source_name = video_metadata.source_name
      source_width = video_metadata.video_stream.width
      source_height = video_metadata.video_stream.height
      duration = video_metadata.video_stream.duration
      key_frames_interval = (eval(video_metadata.video_stream.avg_frame_rate) * 2).ceil


      static_params = "#{STATIC_PARAMS} -g #{key_frames_interval} -keyint_min #{key_frames_interval} -hls_time #{SEGMENT_TARGET_DURATION}"
      master_playlist = ""
      cmd = ""

      RENDER_CONFIGURATIONS.each do | config |
        config_width = config[:resolution].split("x").first.to_i
        config_height = config[:resolution].split("x").last.to_i

        # config[:bitrate] = (config_width * config_height * eval(video_metadata.video_stream.avg_frame_rate).to_i * 0.07) / 1000
        config[:bitrate] = config[:bitrate].to_i

        maxrate = (config[:bitrate] * MAX_BITRATE_RATIO).to_i
        bufsize = (config[:bitrate] * RATE_MONITOR_BUFFER).to_i
        bandwidth = config[:bitrate] * 1000

        if source_height >= config_height
          if video_metadata.is_portrait?
            config_width, config_height = config_height, config_width
          end

          name = "#{source_name}_#{config_height}p"

          cmd += " #{static_params} -vf scale=w=#{config_width}:h=#{config_height}:force_original_aspect_ratio=decrease"
          cmd += " -b:v #{config[:bitrate]} -maxrate #{maxrate}k -bufsize #{bufsize}k -b:a #{config[:audiorate]}"
          cmd += " -hls_segment_filename #{source_path}/#{name}_%05d.ts #{source_path}/#{name}.m3u8"

          master_playlist += "#EXT-X-STREAM-INF:BANDWIDTH=#{bandwidth},RESOLUTION=#{config[:resolution]}\n#{name}.m3u8\n"
        end
      end

      # CommandLine.run("ffmpeg -loglevel error -hide_banner -y -i #{source} #{cmd}")
      # self.playlist(source_path, master_playlist)
      self.thumbnails(source)
    end

    def playlist(source_path, text)
      playlist_header = "#EXTM3U\n#EXT-X-VERSION:3\n"

      File.open("#{source_path}/playlist.m3u8", "wb") do |f|
        f.write(playlist_header + text)
      end
    end

    def thumbnails(source)
      video_metadata = VideoMetadata.new(source)

      source_path = video_metadata.source_path
      source_name = video_metadata.source_name

      if video_metadata.is_portrait?
        filter = "-filter_complex 'scale=ih*16/9:-1,crop=h=iw*9/16,boxblur=luma_radius=min(h\\,w)/20:luma_power=1:chroma_radius=min(cw\\,ch)/20:chroma_power=1[bg];[bg][0:v]overlay=(W-w)/2:(H-h)/2'"
      else
        filter = "-vf fps=12/60"
      end

      params  = " #{filter} -s 160x90 #{source_path}/images/#{source_name}-160x90-%05d.jpg"
      params += " #{filter} -s 320x180 #{source_path}/images/#{source_name}-320x180-%05d.jpg"
      params += " #{filter} -s 640x360 #{source_path}/images/#{source_name}-640x360-%05d.jpg"
      # params = " -lavfi '[0:v]scale=ih*16/9:-1,boxblur=luma_radius=min(h\\,w)/20:luma_power=1:chroma_radius=min(cw\\,ch)/20:chroma_power=1[bg];[bg][0:v]overlay=(W-w)/2:(H-h)/2,crop=h=iw*9/16' -s 640x360 #{source_path}/images/#{source_name}-640x360-%05d.jpg"

       puts "ffmpeg -loglevel error -hide_banner -y -i #{source} #{params}"
      CommandLine.run("mkdir -p #{source_path}/images")
      CommandLine.run("ffmpeg -loglevel error -hide_banner -y -i #{source} #{params}")
    end
  end
end
