class TranscodeLog < ApplicationRecord
  default_scope { order(created_at: :desc) }

  enum status: [:submitted, :queued, :started, :downloaded, :transcoded, :uploaded, :completed, :failed]

  def self.create_log(transcode_key, status)
    self.create(transcode_key: transcode_key, status: status)
  end
end
