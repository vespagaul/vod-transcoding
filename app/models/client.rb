class Client < ActiveRecord::Base
  validates :name, :bucket_type, :input_bucket_name, :output_bucket_name, presence: true
  validates_presence_of :token, on: :update
  enum bucket_type: [:gcs, :aws]

  before_validation :generate_token, on: :create

  private
  def generate_token
    self.token = SecureRandom.hex(16) if self.token.nil?
  end
end
