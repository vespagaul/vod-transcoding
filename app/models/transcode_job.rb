class TranscodeJob < ActiveRecord::Base
  default_scope { order(created_at: :desc) }
  validates :transcode_key, :video_key, :client_token, presence: true
  self.primary_key = "transcode_key"

  after_create :create_log

  has_many :transcode_logs, foreign_key: "transcode_key"

  def status
    self.transcode_logs.first.status
  end
  private
  def create_log
    TranscodeLog.create_log(self.transcode_key, "submitted")
  end
end
