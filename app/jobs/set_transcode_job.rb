class SetTranscodeJob < ApplicationJob
  # Set the Queue as Default
  queue_as :default

  def perform(transcode_key)
    job = TranscodeNode.where(status: 'ready').first
    if job.present?
      job.transcode_key = transcode_key
      job.status = 'busy'
      job.save
    else
      retry_job wait: 1.minute, queue: :default
    end
  end
end
