class TranscodeJobsController < ApplicationController
  before_action :check_api_auth_key
  before_action :check_client_token

  def create
    transcode_job = TranscodeJob.new transcode_job_params
    if transcode_job.save
      return render json: { transcode_key: transcode_job.transcode_key }, status: :created
    else
      return render status: :unprocessable_entity
    end
  end

  private

  def check_api_auth_key
    return render status: :unauthorized unless request.headers['X-API-Auth'] == "UkgrhDDgQwxQfkNfZbCJmWJbGTLd3ETy"
  end

  def check_client_token
    client_token = params[:client_token].to_s.strip
    return render status: :unauthorized unless client_token.present?
    return render status: :unauthorized unless Client.where(token: client_token).first.present?
  end

  def transcode_job_params
    params.permit(:transcode_key, :video_key, :client_token).merge(set_transcode_key)
  end

  def set_transcode_key
    { transcode_key: SecureRandom.hex }
  end
end
