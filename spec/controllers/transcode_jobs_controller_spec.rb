require "rails_helper"

describe TranscodeJobsController do
  describe "POST /create" do
    context "without auth header" do
      it "will have status unauthorized" do
        post :create

        expect(response).to have_http_status :unauthorized
      end
    end

    context "with header auth" do
      before do
        request.headers['X-API-Auth'] = "UkgrhDDgQwxQfkNfZbCJmWJbGTLd3ETy"
      end

      context "invalid params" do
        let(:client) { create :client }

        it "response with unprocessable_entity" do
          post :create, params: {
            client_token: client.token
          }

          expect(response).to have_http_status :unprocessable_entity
        end
      end

      context "valid params" do
        context "invalid user token" do
          it "response with http status unauthorized" do
            post :create, params: {
              video_key: 'folder/filename.mp4',
              client_token: 'user-token'
            }

            expect(response).to have_http_status :unauthorized
          end
        end

        context "valid user token" do
          let(:client) { create :client }

          it "response with http created (201)" do
            post :create, params: {
              video_key: 'folder/filename.mp4',
              client_token: client.token
            }

            expect(response).to have_http_status :created

            json = JSON.parse(response.body)
            expect(json['transcode_key']).to be_present
          end
        end
      end
    end
  end
end
