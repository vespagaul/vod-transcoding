require "rails_helper"

describe "Transcode Jobs Routing" do
  it "will have routing" do
    expect(post transcode_jobs_path).to route_to controller: "transcode_jobs", action: "create"
  end
end
