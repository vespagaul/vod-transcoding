FactoryBot.define do
  factory :transcode_log do
    status { Random.rand(1,2) }
    description { Faker::Lorem.word }
  end
end
