FactoryBot.define do
  factory :transcode_job do
    transcode_key { SecureRandom.hex(16) }
    video_key { Faker::Lorem.word }
    client_token { SecureRandom.hex(8) }
  end
end
