FactoryBot.define do
  factory :client do
    name { Faker::Lorem.word }
    bucket_type { 1 }
    input_bucket_name { Faker::Lorem.word }
    output_bucket_name { Faker::Lorem.word }
  end
end
