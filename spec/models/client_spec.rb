require "rails_helper"

describe Client do
  it "has columns" do
    should have_db_column(:name).of_type(:string)
    should have_db_column(:token).of_type(:string)
    should define_enum_for(:bucket_type).with([:gcs, :aws])

    should have_db_column(:input_bucket_name).of_type(:string)
    should have_db_column(:input_bucket_project_id).of_type(:string)
    should have_db_column(:input_bucket_credentials).of_type(:text)

    should have_db_column(:output_bucket_name).of_type(:string)
    should have_db_column(:output_bucket_project_id).of_type(:string)
    should have_db_column(:output_bucket_credentials).of_type(:text)

    should have_db_column(:cdn_url).of_type(:string)
    should have_db_column(:notification_url).of_type(:string)
  end

  it "has validation" do
    is_expected.to validate_presence_of :name
    is_expected.to validate_presence_of :bucket_type

    is_expected.to validate_presence_of :input_bucket_name
    is_expected.to validate_presence_of :output_bucket_name
  end

  describe "create" do
    it "generate token" do
      client = Client.new
      client.name = 'bbm'
      client.bucket_type = 1
      client.input_bucket_name = 'input bucket'
      client.output_bucket_name = 'output bucket'
      client.save

      expect(client.token).not_to be_empty

      client.token = ''
      client.save
      expect(client.errors.messages[:token]).not_to be_empty
    end
  end

  describe "update" do
    it "validate token should not to be empty" do
      client = create(:client)
      client.token = ''
      client.save

      expect(client.errors.messages[:token]).not_to be_empty
    end
  end
end
