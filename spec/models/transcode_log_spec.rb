require 'rails_helper'

describe TranscodeLog do
  it "has columns" do
    should have_db_column(:transcode_key).of_type(:string)
    should have_db_column(:status).of_type(:integer)
    should have_db_column(:description).of_type(:text)

    should define_enum_for(:status).with([:submitted, :queued, :started, :downloaded, :transcoded, :uploaded, :completed, :failed])
  end

  describe ".create_log" do
    it "insert to transcode_log" do
      TranscodeLog.create_log("transcode-key", 0)
      last_log = TranscodeLog.last

      expect(last_log.transcode_key).to eq("transcode-key")
      expect(last_log.status).to eq("submitted")
    end
  end
end
