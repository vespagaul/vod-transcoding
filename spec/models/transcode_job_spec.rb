require "rails_helper"

describe TranscodeJob do
  it "has columns" do
    should have_db_column(:transcode_key).of_type(:string)
    should have_db_column(:video_key).of_type(:string)
    should have_db_column(:client_token).of_type(:string)
  end

  it "has validation" do
    is_expected.to validate_presence_of :video_key
    is_expected.to validate_presence_of :transcode_key
    is_expected.to validate_presence_of :client_token
  end

  it "has associations" do
    is_expected.to have_many :transcode_logs
  end


  describe "after_create" do
    it "create transcode_log" do
      job = create :transcode_job
      last_log = TranscodeLog.last
      expect(last_log.transcode_key).to eq(job.transcode_key)
    end
  end
end
