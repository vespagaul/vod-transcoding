require "google/cloud/pubsub"

namespace :transcoder do
  desc "Publish Message to Google PubSub"
  task pubsub: :environment do
    pubsub = Google::Cloud::Pubsub.new(
      project_id: "bbm-dev",
      credentials: "/home/woi/bbm-dev-37545a31a731.json"
    )

    # Retrieve a topic
    topic = pubsub.topic "pek-transcoder"

    # Publish a new message
    topic.publish "new-message-1"
    topic.publish "new-message-2"
    topic.publish "new-message-3"

    # Retrieve a subscription
    sub = pubsub.subscription "jobs"

    # Create a subscriber to listen for available messages
    puts "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
    subscriber = sub.listen do |received_message|
      # process message
      puts ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
      puts received_message
      puts ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
      # received_message.acknowledge!
    end

    # Start background threads that will call the block passed to listen.
    # subscriber.start

    # Shut down the subscriber when ready to stop receiving messages.
    # subscriber.stop.wait!
  end
end
