RailsAdmin.config do |config|
  config.actions do
    dashboard
    index

    new do
      except ['TranscodeJob', 'TranscodeLog']
    end

    show do
      except ['TranscodeLog']
    end

    edit do
      except ['TranscodeJob', 'TranscodeLog']
    end

    delete do
      except ['TranscodeJob', 'TranscodeLog']
    end
  end

  config.model 'Client' do
    create do
      exclude_fields :token
    end
  end

  config.model 'TranscodeJob' do
    list do
      sort_by :created_at
    end
  end

  config.model 'TranscodeLog' do
    list do
      sort_by :created_at
    end
  end
end
